﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.WSA.Input;

public class ControllerInput : MonoBehaviour
{

	// Use this for initialization
	void Start()
	{

		InteractionManager.InteractionSourceDetected += InteractionManager_SourceDetected;
		InteractionManager.InteractionSourceUpdated += InteractionManager_SourceUpdated;
		InteractionManager.InteractionSourceLost += InteractionManager_SourceLost;
		InteractionManager.InteractionSourcePressed += InteractionManager_SourcePressed;
		InteractionManager.InteractionSourceReleased += InteractionManager_SourceReleased;
	}

	void OnDestroy()
	{

		InteractionManager.InteractionSourceDetected -= InteractionManager_SourceDetected;
		InteractionManager.InteractionSourceUpdated -= InteractionManager_SourceUpdated;
		InteractionManager.InteractionSourceLost -= InteractionManager_SourceLost;
		InteractionManager.InteractionSourcePressed -= InteractionManager_SourcePressed;
		InteractionManager.InteractionSourceReleased -= InteractionManager_SourceReleased;
	}
	// Update is called once per frame
	void Update()
	{
		//Vector3 leftPosition = InputTracking.GetLocalPosition(XRNode.LeftHand);

		//Debug.Log("LP" + leftPosition);


		var interactionSourceStates = InteractionManager.GetCurrentReading();
		Debug.Log("Num Interaction Source States: " + interactionSourceStates.Length);
		/*
		if (interactionSourceStates.selectPressed)
		{
			Debug.Log("Press" + Time.time);
		}
		*/
		if (Input.GetButton("Fire1"))
		{
			Debug.Log("Fire1");
		}

	}

	void InteractionManager_SourcePressed(InteractionSourcePressedEventArgs args)
	{

		Debug.Log("Source pressed");
	}
	void InteractionManager_SourceDetected(InteractionSourceDetectedEventArgs args)
	{

		Debug.Log("Source detected");
	}
	void InteractionManager_SourceUpdated(InteractionSourceUpdatedEventArgs args)
	{
		Vector3 p;
		//float a;
		args.state.sourcePose.TryGetPosition(out p);
		//a = args.state.sourcePose.positionAccuracy;

		//Debug.Log("Source updated " + p);
	}
	void InteractionManager_SourceLost(InteractionSourceLostEventArgs args)
	{

		Debug.Log("Source lost");
	}
	void InteractionManager_SourceReleased(InteractionSourceReleasedEventArgs args)
	{

		Debug.Log("Source released");
	}
}