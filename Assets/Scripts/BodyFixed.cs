using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyFixed : MonoBehaviour
{
	private Quaternion rotation;
	private Vector3 position;

	void Awake()
	{
		//position = transform.localPosition;
		position = new Vector3(gameObject.GetComponent<AppManager>().app_x, 0.0f, 1.0f);
		rotation = transform.rotation;
	}

	private void OnEnable()
	{
		position = new Vector3(gameObject.GetComponent<AppManager>().app_x, 0.0f, 1.0f);
		rotation = transform.rotation;

	}
	void LateUpdate()
	{
		transform.position = Camera.main.transform.position + position;
		transform.rotation= rotation;
	}
}
