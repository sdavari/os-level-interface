﻿using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.Utilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttachWristMenu2Wrist : MonoBehaviour
{
	public GameObject wristObjectL;
	public GameObject wristObjectR;
	MixedRealityPose pose;

	/*
	public void OnSourceDetected(SourceStateEventData eventData)
	{
		var hand = eventData.Controller as IMixedRealityHand;
		if (hand != null)
		{
			if (HandJointUtils.TryGetJointPose(TrackedHandJoint.Wrist, Handedness.Left, out pose))
			{
				wristObjectR.transform.position = pose.Position;
				wristObjectR.SetActive(true);
			}
			else
			{
				wristObjectR.SetActive(false);
			}
			if (HandJointUtils.TryGetJointPose(TrackedHandJoint.Wrist, Handedness.Right, out pose))
			//if (HandJointUtils.TryGetJointPose(TrackedHandJoint.Wrist, Handedness.Both, out pose))
			{
				wristObjectL.transform.position = pose.Position;
				wristObjectL.SetActive(true);
			}
			else
			{
				wristObjectL.SetActive(false);
			}
		}
		else
		{
			wristObjectR.SetActive(false);
			wristObjectL.SetActive(false);
		}
	}
	*/

	// Update is called once per frame
	void Update()
    {
		if (HandJointUtils.TryGetJointPose(TrackedHandJoint.Wrist, Handedness.Right, out pose))
		{
			wristObjectR.transform.position = pose.Position;
			wristObjectR.SetActive(true);
		}
		else
		{
			//wristObjectR.SetActive(false);
		}
		if (HandJointUtils.TryGetJointPose(TrackedHandJoint.Wrist, Handedness.Left, out pose))
		{
			wristObjectL.transform.position = pose.Position;
			wristObjectL.SetActive(true);
		}
		else
		{
			//wristObjectL.SetActive(false);
		}

	}
}
