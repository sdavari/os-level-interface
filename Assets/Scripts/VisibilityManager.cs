﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Security.Policy;
using UnityEngine;
using System;
using Debug = UnityEngine.Debug;
using UnityEngine.XR.WSA.Input;

public class VisibilityManager : MonoBehaviour
{
	private GameObject[] headZoneApps;
	private GameObject[] bodyZoneApps;
	private GameObject[] worldZoneApps;
	//private GameObject[] menueZoneApps;
	private bool headZoneVisibility;
	private bool bodyZoneVisibility;
	private bool worldZoneVisibility;
	//private bool menueZoneVisibility;

	void Start()
	{		
		headZoneApps = FindObjsInScene_tag("HeadZone").ToArray();
		headZoneVisibility = true;
		bodyZoneApps = FindObjsInScene_tag("BodyZone").ToArray();
		bodyZoneVisibility = true;
		worldZoneApps = FindObjsInScene_tag("WorldZone").ToArray();
		worldZoneVisibility = true;
		//menuedZoneApps = GameObject.FindGameObjectsWithTag("menueZone");
		//menueZoneVisibility = true;

	}

	private void Update()
	{
		FindObjInScene_name("AlwaysMenue").SetActive(true);
	}

	public void switch_visibility_zone(string zone)
	{
		switch (zone)
		{
			case "HeadZone":
				switch_visibility_headZone();
				break;
			case "BodyZone":
				switch_visibility_bodyZone();
				break;
			case "WorldZone":
				switch_visibility_worldZone();
				break;
			//case "WorldZone":
				//switch_visibility_menuezones();
				//break;

		}
	}

	public void switch_visibility_all()
	{
		switch_visibility_headZone();
		switch_visibility_bodyZone();
		switch_visibility_worldZone();
		//switch_visibility_menuezones();
	}

	public void switch_visibility_all(bool visibility = false)
	{
		Debug.Log("Reality!");
		// set all to true since they will switch in methods
		headZoneVisibility = !visibility;
		bodyZoneVisibility = !visibility;
		worldZoneVisibility = !visibility;
		//menueZoneVisibility = !visibility;
		switch_visibility_headZone();
		switch_visibility_bodyZone();
		switch_visibility_worldZone();
		//switch_visibility_menuezones();
		Debug.Log("Reality!");

	}

	// Head Zone
	public void switch_visibility_headZone()
	{
		headZoneVisibility = !headZoneVisibility;
		switch_visibility_headZone(headZoneVisibility);

	}

	public void switch_visibility_headZone(bool visibilityFlag)
	{
		//update visibility flag
		headZoneVisibility = visibilityFlag;

		// update Sprite
		GameObject visibilityIcon = FindObjInScene_name("HeadZoneVisibilityIcon");
		update_visibility_sprite(visibilityIcon, headZoneVisibility);

		// Update HeadZoneApps
		headZoneApps = FindObjsInScene_tag("HeadZone").ToArray();
		// update individual app visibility
		foreach (GameObject headZoneApp in headZoneApps)
		{
			GameObject AppvisibilityIcon = AppManager.GetChildWithName(headZoneApp, "AppVisibilityIcon");
			update_visibility_sprite(AppvisibilityIcon, headZoneVisibility);
			headZoneApp.GetComponent<AppManager>().update_visibility(headZoneVisibility);
		}
	}

	// Body Zone
	public void switch_visibility_bodyZone()
	{
		Debug.Log("Body Zone Visibility Switch Start");
		bodyZoneVisibility = !bodyZoneVisibility;
		switch_visibility_bodyZone(bodyZoneVisibility);
		Debug.Log("Body Zone Visibility is now: " + bodyZoneVisibility);

	}

	public void switch_visibility_bodyZone(bool visibilityFlag)
	{
		//update visibility flag
		bodyZoneVisibility = visibilityFlag;

		// update Sprite
		GameObject visibilityIcon = FindObjInScene_name("BodyZoneVisibilityIcon");
		update_visibility_sprite(visibilityIcon, bodyZoneVisibility);
		bodyZoneApps = FindObjsInScene_tag("BodyZone").ToArray();
		// update individual app visibility
		foreach (GameObject bodyZoneApp in bodyZoneApps)
		{
			GameObject AppvisibilityIcon = AppManager.GetChildWithName(bodyZoneApp, "AppVisibilityIcon");
			update_visibility_sprite(AppvisibilityIcon, bodyZoneVisibility);
			bodyZoneApp.GetComponent<AppManager>().update_visibility(bodyZoneVisibility);
		}
	}

	// World Zone
	public void switch_visibility_worldZone()
	{
		Debug.Log("World Zone Visibility Switch Start");
		//update visibility flag
		worldZoneVisibility = !worldZoneVisibility;
		switch_visibility_worldZone(worldZoneVisibility);
		Debug.Log("World Zone Visibility is now: " + worldZoneVisibility );
	}

	public void switch_visibility_worldZone(bool visibilityFlag)
	{
		//update visibility flag
		worldZoneVisibility = visibilityFlag;

		// update Sprite
		GameObject visibilityIcon = FindObjInScene_name("WorldZoneVisibilityIcon");
		update_visibility_sprite(visibilityIcon, worldZoneVisibility);
		worldZoneApps = FindObjsInScene_tag("WorldZone").ToArray();
		// update individual app visibility
		foreach (GameObject worldZoneApp in worldZoneApps)
		{
			GameObject AppvisibilityIcon = AppManager.GetChildWithName(worldZoneApp, "AppVisibilityIcon");
			update_visibility_sprite(AppvisibilityIcon, worldZoneVisibility);
			worldZoneApp.GetComponent<AppManager>().update_visibility(worldZoneVisibility);
		}
	}
	
	// Sprite
	public static void update_visibility_sprite(GameObject visibilityIcon, bool visibility)
	{
		Sprite visibilitySprite;
		if (visibility)
		{
			visibilitySprite = Resources.Load<Sprite>("signs/VE-Visibility");
			//Debug.Log("Sprite visible: " + visibilityIcon.gameObject.name);
		}
		else
		{
			visibilitySprite = Resources.Load<Sprite>("signs/VE-Visibility-deActive");
			//Debug.Log("Sprite invisible " + visibilityIcon.gameObject.name);

		}
		
		visibilityIcon.GetComponent<SpriteRenderer>().sprite = visibilitySprite;
	}

	public static GameObject FindObjInScene_name(string name)
	{
		GameObject[] allObjects = Resources.FindObjectsOfTypeAll<GameObject>();
		foreach (GameObject obj in allObjects)
			if (obj.name == name)
				return obj;
		return null;
	}

	public static List<GameObject> FindObjsInScene_tag(string tag = "all")
	{
		List<GameObject> goList = new List<GameObject>();
		GameObject[] allObjects = Resources.FindObjectsOfTypeAll<GameObject>();
		if (tag == "all")
		{
			foreach (GameObject obj in allObjects)
				if (obj.tag == "HeadZone" || obj.tag == "BodyZone" || obj.tag == "WorldZone")
					goList.Add(obj);
		}
		else
		{
			foreach (GameObject obj in allObjects)
				if (obj.tag == tag)
					goList.Add(obj);
		}
		return goList;
	}
}
