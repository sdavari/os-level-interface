﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Microsoft.MixedReality.Toolkit.Utilities.Solvers;
using Microsoft.MixedReality.Toolkit.Input;
using HoloToolkit.Unity.InputModule;

public class AppManager : MonoBehaviour
{
	private bool visible;
	public float app_x;

	void Start()
	{
		visible = true;
	}

	public void update_visibility()
	{
		visible = !visible;
		switch_object_visibility(gameObject, visible);
	}

	public void update_visibility(bool zoneVisibility)
	{
		visible = zoneVisibility;
		gameObject.SetActive(visible);
		//switch_object_visibility(gameObject, visible);
	}

	public void update_zone(string targetZone = null)
	{

		if (targetZone == null || targetZone == "")
		{
			if (gameObject.tag != null)
			{
				switch (gameObject.tag)
				{
					case "BodyZone":
						targetZone = "WorldZone";
						break;
					case "WorldZone":
						targetZone = "HeadZone";
						break;
					case "HeadZone":
						targetZone = "BodyZone";
						break;
					default:
						targetZone = "BodyZone";
						break;
				}
			}
			else
				targetZone = "BodyZone";
		}
		gameObject.tag = targetZone;
		// update Sprite
		GameObject fixationIcon = GetChildWithName(gameObject, "Fixation");
		update_fixation_sprite(fixationIcon, targetZone);
		update_fixation();
	}

	private void update_fixation()
	{
		string name;
		GameObject gm;
		Vector3 v, v2;
		switch (gameObject.tag)
		{
			case "HeadZone":
				gameObject.GetComponent<BodyFixed>().enabled = false;
				name = gameObject.name;
				//transform.localPosition = new Vector3(Click2Open.headZoneX, 0.4f, -0.1f);
				gm = Instantiate(gameObject, VisibilityManager.FindObjInScene_name("HeadFixedApps").gameObject.transform);
				gm.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("AppsScreenshots/" + name + "_HeadZone");

				v = new Vector3(Click2Open.headZoneX, 0.1f, -1.0f);
				gm.transform.localPosition = v;
				v2 = new Vector3(0.0f, 5.0f, 0.0f);
				gm.transform.Find("AppTop").transform.localPosition = v2;
				gm.transform.localScale /= 2;
				//gm.transform.position = new Vector3(gm.transform.position.x, 1.0f, 2.0f);
				Click2Open.headZoneX -= 0.3f;
				gm.name = name;
				gm.tag = gameObject.tag;
				Destroy(gameObject);
				break;
			case "BodyZone":
				if (!gameObject.GetComponent<BodyFixed>())
					gameObject.AddComponent<BodyFixed>();
				gameObject.GetComponent<BodyFixed>().enabled = true;
				name = gameObject.name;
				//transform.localPosition = Camera.main.transform.position + new Vector3(Click2Open.bodyZoneX, 0.0f, 2.5f);
				gm = Instantiate(gameObject, VisibilityManager.FindObjInScene_name("Applications").gameObject.transform);
				gm.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("AppsScreenshots/" + name.Substring(0,3) );
				v = new Vector3(app_x, 0.0f, 1.0f);
				gm.transform.localPosition = v;
				v2 = new Vector3(0.0f, 6.0f, 0.0f);
				gm.transform.Find("AppTop").transform.localPosition = v2;
				gm.GetComponent<BodyFixed>().enabled = true;
				gm.transform.localScale *= 2;
				gm.name = name;
				gm.tag = gameObject.tag;
				//Click2Open.bodyZoneX -= 0.8f;
				Destroy(gameObject);
				break;
			case "WorldZone":
				gameObject.GetComponent<BodyFixed>().enabled = false;
				break;
		}
}

	private void switch_object_visibility(GameObject go, bool vis)
	{
		go.SetActive(vis);
		//foreach( SpriteRenderer sr in GetComponentsInChildren<SpriteRenderer>())
		//sr.enabled = vis;
	}

	public void switch_visibility()
	{
		visible = !visible;
		switch_visibility(visible);
	}

	public void switch_visibility(bool visibilityFlag)
	{
		//update visibility flag
		visible = visibilityFlag;

		// update Sprite
		GameObject visibilityIcon = GetChildWithName(gameObject, "AppVisibilityIcon");
		VisibilityManager.update_visibility_sprite(visibilityIcon, visible);
		gameObject.GetComponent<AppManager>().update_visibility(visible);

	}

	public static GameObject GetChildWithName(GameObject obj, string name)
	{
		foreach (Transform eachChild in obj.transform)
		{
			if (eachChild.name == name)
			{
				return eachChild.gameObject;
			}
			else if (eachChild.transform.Find(name))
				return eachChild.transform.Find(name).gameObject;
		}

		return null;
	}

	private void update_fixation_sprite(GameObject fixationIcon, string targetZone)
	{
		Sprite fixationSprite;
		fixationSprite = Resources.Load<Sprite>("signs/"+targetZone);

		fixationIcon.GetComponent<SpriteRenderer>().sprite = fixationSprite;
	}
}