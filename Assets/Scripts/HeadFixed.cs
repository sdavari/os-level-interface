﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadFixed : MonoBehaviour
{
	private Quaternion rotation;
	private Vector3 position;

	void Awake()
	{
		position = transform.localPosition;
		//rotation = transform.localRotation;
	}
	void LateUpdate()
	{
		transform.position = Camera.main.transform.position + position;
		transform.rotation = Camera.main.transform.rotation;
	}
}
