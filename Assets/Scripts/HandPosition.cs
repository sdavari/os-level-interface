﻿using UnityEngine;
using UnityEngine.XR.WSA.Input;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.Utilities;
using Microsoft.MixedReality.Toolkit;
using System;

public class HandPosition : MonoBehaviour
{
		
	public void Update()
	{
		if (HandJointUtils.TryGetJointPose(TrackedHandJoint.MiddleMetacarpal, Handedness.Left, out MixedRealityPose jointPose))
		{
			Debug.Log("jointPose: " + jointPose);
			this.transform.parent.position = jointPose.Position;
		}
	
	}

}
