﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Click2Close : MonoBehaviour
{
	public GameObject current_screen;
	public bool isMenu;

	public void clicked()
	{
		if (isMenu)
			current_screen.SetActive(false);
		else
			Destroy(current_screen);
	}
	
}
