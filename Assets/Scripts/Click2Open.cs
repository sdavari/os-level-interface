﻿using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.Utilities.Solvers;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Click2Open : MonoBehaviour
{
	public static bool notif_on = true;
	public GameObject current_screen;
	public static float bodyZoneX = 0.40f;
	public static float headZoneX = 0.1f;
	private string zone;
	private string appName;
	private bool isApp;

	private void Start()
	{
		appName = gameObject.name;
		zone = "Untagged";
		// make the app
		switch (appName)
		{
			case "Apps":
				appName = "AppsList";
				isApp = false;
				break;
			case "Zones":
				appName = "ZonesSetting";
				isApp = false;
				break;
			case "HeadZoneMenuPart":
				appName = "HeadZoneMenuBG";
				update_apps_Lists();
				isApp = false;
				break;
			case "BodyZoneMenuPart":
				appName = "BodyZoneMenuBG";
				update_apps_Lists();
				isApp = false;
				break;
			case "WorldZoneMenuPart":
				appName = "WorldZoneMenuBG";
				update_apps_Lists();
				isApp = false;
				break;
			case "Gmail":
				appName = "Gmail0";
				zone = "BodyZone";
				isApp = true;
				break;
			case "Gmail0":
				appName = "Gmail1";
				zone = "BodyZone";
				isApp = true;
				break;
			default:
				zone = "BodyZone";
				isApp = true;
				break;
		}
	}

	public void set_current_screen(GameObject cs)
	{
		current_screen = cs;
	}

	public void update_apps_Lists()
	{
		List<GameObject> apps = null;
		GameObject parent = null;
		GameObject sampleObj = VisibilityManager.FindObjInScene_name("SampleIcon");
		switch (appName)
		{
			
			case "HeadZoneMenuBG":
				apps = VisibilityManager.FindObjsInScene_tag("HeadZone");
				parent = VisibilityManager.FindObjInScene_name("HeadZoneIcons");
				break;
			case "BodyZoneMenuBG":
				apps = VisibilityManager.FindObjsInScene_tag("BodyZone");
				parent = VisibilityManager.FindObjInScene_name("BodyZoneIcons");
				break;
			case "WorldZoneMenuBG":
				apps = VisibilityManager.FindObjsInScene_tag("WorldZone");
				parent = VisibilityManager.FindObjInScene_name("WorldZoneIcons");
				break;
			default:
				return;
		}
		// clear all of the apps in the list
		foreach (Transform child in parent.transform)
		{
			GameObject.Destroy(child.gameObject);
		}

		// add icons and display them
		int icon_id = 0;
		foreach (GameObject go in apps)
		{
			float x = (icon_id % 3) - 1;
			float y = -1 * ((int)(icon_id / 3) - 1);
			icon_id++;
			Vector3 v = new Vector3(x, y, 0.0f);
			GameObject gm = Instantiate(sampleObj, parent.transform);
			gm.transform.localPosition = v;
			gm.SetActive(true);
			if (go.name == "Gmail0" || go.name == "Gmail1")
				gm.name = go.name.Substring(0, go.name.Length - 1);
			else
				gm.name = go.name;
			// Update Sprite
			Debug.Log(gm.name);
			SpriteRenderer _spRenderer = gm.GetComponent<SpriteRenderer>();
			_spRenderer.enabled = true;
			_spRenderer.sprite = Resources.Load<Sprite>("icons/" + gm.name);
			gm.GetComponent<Click2Open>().set_current_screen(VisibilityManager.FindObjInScene_name(appName));
		}
		Debug.Log(parent.name);
		//display_apps_Lists(parent);
	}

	public void clicked()
	{
		hide_non_apps();
		if (!isApp)
		{
			update_apps_Lists();
			VisibilityManager.FindObjInScene_name(appName).GetComponent<AppManager>().update_visibility(true);

		}
		else
		{
			if (appName != "Gmail1")
			{
				current_screen.GetComponent<AppManager>().update_visibility(false);
				CreateApp().GetComponent<AppManager>().update_visibility(true);
			}
			else
			{
				notif_on = false;
				VisibilityManager.FindObjInScene_name("1").SetActive(false);
				//gameObject.SetChildrenActive(false);
				gameObject.GetComponent<Click2Open>().enabled = false;
				gameObject.GetComponent<InputActionHandler>().enabled = false;
				SpriteRenderer _spRenderer = GetComponent<SpriteRenderer>();
				_spRenderer.enabled = true;
				_spRenderer.sprite = Resources.Load<Sprite>("AppsScreenshots/" + appName);
			}
		}
	}

	private GameObject CreateApp()
	{
		Transform parent_trans;
		GameObject gm;
		GameObject sampleObj = VisibilityManager.FindObjInScene_name("SampleApp");
		if (zone == "HeadZone")
		{
			parent_trans = VisibilityManager.FindObjInScene_name("HeadFixedApps").gameObject.transform;
			//sampleObj.transform.position = new Vector3(headZoneX, 0.30f, 1.0f);
			gm = Instantiate(sampleObj, parent_trans);
			Vector3 v = new Vector3(headZoneX, 0.1f, -1.0f);
			gm.transform.localPosition = v;
			gm.GetComponent<AppManager>().app_x = gm.transform.localPosition.x;
			Vector3 v2 = new Vector3(0.0f, 5.0f, 0.0f);
			gm.transform.Find("AppTop").transform.localPosition = v2;
			gm.transform.localScale /= 2;
			headZoneX -= 0.3f;
		}
		else 
		{
			parent_trans = VisibilityManager.FindObjInScene_name("Applications").gameObject.transform;
			gm = Instantiate(sampleObj, parent_trans);
			Debug.Log(bodyZoneX);
			Vector3 v = new Vector3(bodyZoneX, 0.0f, 1.0f);
			gm.transform.localPosition = v; //(v.x, v.y, v.z);
			Debug.Log(gm.transform.localPosition);
			gm.GetComponent<AppManager>().app_x = gm.transform.localPosition.x;
			Vector3 v2 = new Vector3(0.0f, 6.0f, 0.0f);
			gm.transform.Find("AppTop").transform.localPosition = v2;
			if (zone == "BodyZone")
			{
				if (!gm.GetComponent<BodyFixed>())
					gm.AddComponent<BodyFixed>();
				gm.GetComponent<BodyFixed>().enabled = true;
			}
			bodyZoneX -= 0.8f;
			Debug.Log(bodyZoneX);

		}
		// set app's Zone
		gm.tag = zone;
		gm.name = appName;

		if (appName == "Gmail0")
		{
			gm.GetComponent<Click2Open>().enabled = true;
			gm.GetComponent<Click2Open>().set_current_screen(gm);
			gm.GetComponent<InputActionHandler>().enabled = true;
		}

		// Update Sprite
		SpriteRenderer _spRenderer = gm.GetComponent<SpriteRenderer>();
		_spRenderer.enabled = true;
		if (zone != "HeadZone")
			_spRenderer.sprite = Resources.Load<Sprite>("AppsScreenshots/" + appName);
		else
		{
			_spRenderer.sprite = Resources.Load<Sprite>("AppsScreenshots/" + appName + "_HeadZone");
		}
		return gm;
	}

	private void hide_non_apps()
	{
		VisibilityManager.FindObjInScene_name("AppsList").GetComponent<AppManager>().update_visibility(false);
		VisibilityManager.FindObjInScene_name("ZonesSetting").GetComponent<AppManager>().update_visibility(false);
		VisibilityManager.FindObjInScene_name("HeadZoneMenuBG").GetComponent<AppManager>().update_visibility(false);
		VisibilityManager.FindObjInScene_name("BodyZoneMenuBG").GetComponent<AppManager>().update_visibility(false);
		VisibilityManager.FindObjInScene_name("WorldZoneMenuBG").GetComponent<AppManager>().update_visibility(false);
	}
}
