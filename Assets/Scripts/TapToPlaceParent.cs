﻿using UnityEngine;

public class TapToPlaceParent : MonoBehaviour
{
	bool placing = false;

	public void check_nan_position()
	{
		if (float.IsNaN(transform.position.x) || float.IsNaN(transform.position.y) || float.IsNaN(transform.position.z))
		{
			transform.position = new Vector3(0, 0, 1.5f);
		}
	}

	// Called by GazeGestureManager when the user performs a Select gesture
	void OnSelect()
	{
		placing = !placing;
		
	}

	// Called by SpeechManager when the user says the "Drop sphere" command
	void OnDrop()
	{
		// Just do the same logic as a Select gesture.
		OnSelect();
	}
	// Update is called once per frame
	void Update()
	{
		// If the user is in placing mode,
		// update the placement to match the user's gaze.
		if (placing)
		{
			// Do a raycast into the world that will only hit the Spatial Mapping mesh.
			var headPosition = Camera.main.transform.position;
			var gazeDirection = Camera.main.transform.forward;

			RaycastHit hitInfo;
			if (Physics.Raycast(headPosition, gazeDirection, out hitInfo,
				30.0f, SpatialMapping.PhysicsRaycastMask))
			{
				// Move this object's parent object to
				// where the raycast hit the Spatial Mapping mesh.
				this.transform.parent.position = hitInfo.point;

				// Rotate this object's parent object to face the user.
				Quaternion toQuat = Camera.main.transform.localRotation;
				toQuat.x = 0;
				toQuat.z = 0;
				this.transform.parent.rotation = toQuat;
			}
		}
	}
}